#include "my_action_initialization.h"

#include "my_primary_generation_action.h"

void MyActionInitialization::Build() const {
    SetUserAction(new MyPrimaryGenerationAction);
}
