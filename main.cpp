#include "G4MTRunManager.hh"

#include "my_detector_construction.h"
#include "my_physics_list.h"
#include "my_action_initialization.h"

#include <thread>

int main(int argc, char *argv[])
{
    auto manager{new G4MTRunManager};

    manager->SetNumberOfThreads(std::thread::hardware_concurrency());
    manager->SetUserInitialization(new MyDetectorConstruction);
    manager->SetUserInitialization(new MyPhysicsList);
    manager->SetUserInitialization(new MyActionInitialization);

    manager->Initialize();

    manager->BeamOn(1);

    return 0;
}
