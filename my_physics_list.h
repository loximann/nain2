#ifndef MY_PHYSICS_LIST_H
#define MY_PHYSICS_LIST_H

#include "G4VModularPhysicsList.hh"

class MyPhysicsList : public G4VModularPhysicsList {
public:
    /// Constructor.
    explicit MyPhysicsList();
};

#endif // MY_PHYSICS_LIST_H

