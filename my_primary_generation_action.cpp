#include "my_primary_generation_action.h"

#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"

MyPrimaryGenerationAction::MyPrimaryGenerationAction() :
    m_gun{1}
{
    m_gun.SetParticleDefinition(G4ParticleTable::GetParticleTable()->FindParticle("e-"));
    m_gun.SetParticleMomentumDirection({0,0,1});
    m_gun.SetParticleEnergy(1 * MeV);
    m_gun.SetParticlePosition({0, 0, 0});
}

void MyPrimaryGenerationAction::GeneratePrimaries(G4Event* event) {
    return this->m_gun.GeneratePrimaryVertex(event);
}


