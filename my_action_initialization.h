#ifndef MY_ACTION_INITIALIZATION_H
#define MY_ACTION_INITIALIZATION_H

#include "G4VUserActionInitialization.hh"

class MyActionInitialization: public G4VUserActionInitialization
{
public:
    MyActionInitialization() = default;
    void Build() const final;
//    void BuildForMaster() const final;
};
#endif // MY_ACTION_INITIALIZATION_H

