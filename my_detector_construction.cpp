#include "my_detector_construction.h"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

G4VPhysicalVolume* MyDetectorConstruction::Construct() {
    G4Material* water = G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");
    return new G4PVPlacement(G4Transform3D{},         // don't rotate, center at origin
                             new G4LogicalVolume{new G4Box{"Box", 10 * m, 10 * m, 10 * m}, water, "World"}, // its logical volume
                             "World",         // its name
                             nullptr,         // its mother  volume
                             false,           // no boolean operation
                             0,               // copy number
                             false);          // checking overlaps
}
