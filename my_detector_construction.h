#ifndef MY_DETECTOR_CONSTRUCTION_H
#define MY_DETECTOR_CONSTRUCTION_H

#include "G4VUserDetectorConstruction.hh"

class MyDetectorConstruction: public G4VUserDetectorConstruction
{
public:
    MyDetectorConstruction () = default;
    virtual ~MyDetectorConstruction () = default;

    G4VPhysicalVolume* Construct() final;
};


#endif // MY_DETECTOR_CONSTRUCTION_H
