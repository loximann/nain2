#ifndef MY_PRIMARY_GENERATION_ACTION_H
#define MY_PRIMARY_GENERATION_ACTION_H

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4ParticleGun.hh"

class MyPrimaryGenerationAction: public G4VUserPrimaryGeneratorAction
{
public:
    MyPrimaryGenerationAction();
    virtual ~MyPrimaryGenerationAction () = default;

    void GeneratePrimaries(G4Event* event) final;
private:
    G4ParticleGun m_gun;
};
#endif // MY_PRIMARY_GENERATION_ACTION_H
