#include "my_physics_list.h"

#include "G4DecayPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"

MyPhysicsList::MyPhysicsList() {
    this->RegisterPhysics(new G4EmStandardPhysics);
    this->RegisterPhysics(new G4HadronPhysicsQGSP_BIC_HP);
    this->RegisterPhysics(new G4DecayPhysics);
}

